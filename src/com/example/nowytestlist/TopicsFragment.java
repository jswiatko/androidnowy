package com.example.nowytestlist;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class TopicsFragment extends ListFragment {	
	
	String[] topics ={
			"EPFL", 
			"PocketCampus", 
			"Balelec", 
			"Justin Bieber",
			"May", 
			"June", 
			"July", 
			"August",
			"September", 
			"October", 
			"November", 
			"December"
	};
	
	 public TopicsFragment() {
     }
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ListAdapter myListAdapter = new ArrayAdapter<String>(
				getActivity(),
				android.R.layout.simple_list_item_1,
				topics);
		setListAdapter(myListAdapter);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.listfragment1, container, false);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Intent intent = new Intent(getActivity(), HelloBubblesActivity.class);	  
	    startActivity(intent);
	}

	

}
